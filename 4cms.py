#!/usr/bin/env python3

import requests
import sys
import json
import os

JSON_SERVER = 'https://a.4cdn.org'
FILE_SERVER = 'https://i.4cdn.org'
TIMEOUT = 5


def get_thread_url(thread):
    if 'thread/' in thread:
        return JSON_SERVER + '/' + thread + '.json'

    board, id = thread.split('/')
    return JSON_SERVER + '/' + board + '/thread/' + id + '.json'


def get_media_url(filename, board):
    return FILE_SERVER + '/' + board + '/' + filename


def get_media_files(content, board):
    files = []
    tree = json.loads(content)
    if 'posts' not in tree:
        return None

    for post in tree['posts']:
        if 'filename' not in post:
            continue
        files.append({'file': str(post['tim']) + post['ext'], \
                      'name': post['filename'], 'board': board, \
                      'ext': post['ext']})

    return files


def download_files(files, output_dir):
    for mediafile in files:
        print("downloading %s..." % mediafile['file'])
        r = requests.get(get_media_url(mediafile['file'], mediafile['board']), timeout=TIMEOUT)
        f = open(output_dir + '/' + mediafile['name'] + mediafile['ext'], 'wb')
        f.write(r.content)
        f.close()


def scraper(output_dir, threads):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    print("output dir: %s" % output_dir)

    for thread in threads:
        print("loading thread: %s..." % thread)
        board, _ = thread.split('/')
        url = get_thread_url(thread)
        content = requests.get(url, timeout=TIMEOUT).content
        filenames = get_media_files(content, board)

        if filenames is None:
            print("no posts in %s" % url)
            continue

        print("now downloading...")
        download_files(filenames, output_dir)


def main():
    if len(sys.argv) < 2:
        print("usage: <output> <thread1>...[threadN]")
        print("thread format: <board>/<id>")
        sys.exit(1)

    output_dir = sys.argv[1]
    threads = sys.argv[2:]

    scraper(output_dir, threads)


if __name__ == '__main__':
    main()
